<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\paginasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', 'paginasController@index');

Route::get('/listagemLivros', 'paginasController@listagemLivros')->name('listagemLivros');
Route::get('/listagemAutores', 'paginasController@listagemAutores')->name('listagemAutores');
Route::get('/listagemEditoras', 'paginasController@listagemEditoras')->name('listagemEditoras');
*/

Route::get('/', function(){
    return view('welcome');
})->name('home');

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Route::get('/listagemAutores', function(){
        $autores = \App\Models\Autores::get();
        return view('listagens\listagemAutores')->with(compact('autores'));
})->name('listagemAutores');

Route::get('/listagemLivros', function(){
        $livros = \App\Models\Livros::with('autor')->get();
        return view('listagens\listagemLivros')->with(compact('livros'));
})->name('listagemLivros');

Route::get('/listagemEditoras', function(){
    $editoras = \App\Models\Editoras::get();
    return view('listagens\listagemEditoras')->with(compact('editoras'));
})->name('listagemEditoras');

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Route::get('/editarAutores', function(){
    $autores = \App\Models\Autores::get();
    return view('cadastros\editarAutores')->with(compact('autores'));
})->name('editarAutores');

Route::get('/editarLivros', function(){
        $livros = \App\Models\Livros::with('autor')->get();
        return view('cadastros\editarLivros')->with(compact('livros'));
})->name('editarLivros');

Route::get('/editarEditoras', function(){
        $editoras = \App\Models\Editoras::get();
        return view('cadastros\editarEditoras')->with(compact('editoras'));
})->name('editarEditoras');

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

Route::get('/editar_autor/{id}', function($id){
        $autor = \App\Models\Autores::find($id);
        if($autor){
                return view('cadastros\editarAutor')->with(compact('autor'));
        } else {
                alert ('Erro ao acessar lançamento!');
                return redirect()->back()->withInput();
        }
        
    })->name('editar_autor');

    Route::get('/editar_editora/{id}', function($id){
        $editora = \App\Models\Editoras::find($id);
        if($editora){
                return view('cadastros\editarEditora')->with(compact('editora'));
        } else {
                alert ('Erro ao acessar lançamento!');
                return redirect()->back()->withInput();
        }
        
    })->name('editar_editora');

    Route::get('/editar_livro/{id}', function($id){
        $livro = \App\Models\Livros::find($id);
        $autores = \App\Models\Autores::get();
        $editoras = \App\Models\Editoras::get();
        if($livro){
                return view('cadastros\editarLivro')->with(compact('livro','autores','editoras' ));
        } else {
                alert ('Erro ao acessar lançamento!');
                return redirect()->back()->withInput();
        }
        
    })->name('editar_livro');



//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        Route::post('/salva_autor', function(Request $request){
                //var_dump($request->all());
                //exit;
                $autor = \App\Models\Autores::find($request->id);
                $autor->nome = $request->nome;
                $autor->save();
                return redirect('editarAutores');
                })->name('salva_autor');

        Route::post('/salva_editora', function(Request $request){
                //var_dump($request->all());
                //exit;
                $editora = \App\Models\Editoras::find($request->id);
                $editora->nome = $request->nome;
                $editora->save();
                return redirect('editarEditoras');
                })->name('salva_editora');

                
       Route::post('/salva_livro',function(Request $request){
                //var_dump($request->all());
                //exit;
                $livro = \App\Models\Livros::find($request->id);
                $livro->titulo = $request->titulo;
                $livro->id_autor = $request->id_autor;
                $livro->id_editora = $request->id_editora;
                $livro->local = $request->local;
                $livro->save();
                return redirect('editarLivros');
                })->name('salva_livro');
                
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        Route::get('/inserir_novo_autor', function(){
                $autor = new \App\Models\Autores;
                $autor->nome = 'novo';
                $autor->save();

                return redirect('editar_autor/' .$autor->id);
                
        })->name('inserir_novo_autor');


        Route::get('/inserir_nova_editora', function(){
                $editora = new \App\Models\Editoras;
                $editora->nome = 'novo';
                $editora->save();

                return redirect('editar_editora/' .$editora->id);
                
        })->name('inserir_nova_editora');

        Route::get('/inserir_novo_livro', function(){
                $livro = new \App\Models\Livros;
                $livro->titulo = 'novo';
                $livro->id_autor = '1';
                $livro->id_editora = '1';
                $livro->local = 'novo';
                $livro->save();

                return redirect('editar_livro/' .$livro->id);
                
        })->name('inserir_novo_livro');
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        Route::get('/deletar_autor/{id}', function($id){
                $livro = \App\Models\Livros::where('id_autor',$id)->first();
                if($livro){
                        return redirect('editarAutores')->with('error',"Existe livro cadastrado para autor selecionado, id: " .$id.". Autor NÃO pode ser deletado");
                }else{
                        $autor = \App\Models\Autores::find($id);
                        $autor->delete();
                        return redirect('editarAutores');
                }

                return redirect('editar_autor/' .$autor->id);
                
        })->name('deletar_autor');


        Route::get('/deletar_editora/{id}', function($id){
                $livro = \App\Models\Livros::where('id_editora',$id)->first();
                if($livro){
                        return redirect('editarEditoras')->with('error',"Existe livro cadastrado para editora selecionada, id: " .$id.". Editora NÃO pode ser deletada");
                }else{
                        $editora = \App\Models\Editoras::find($id);
                        $editora->delete();
                        return redirect('editarEditoras');
                }

                return redirect('editar_editora/' .$editora->id);
                
        })->name('deletar_editora');

        Route::get('/deletar_livro/{id}', function($id){
                        $livro = \App\Models\Livros::find($id);
                        $livro->delete();
                        return redirect('editarLivros');

                return redirect('editar_livro/' .$livro->id);
                
        })->name('deletar_livro');