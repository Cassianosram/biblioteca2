@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edição de Livros</title>
        <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
        <style>
            .btn-custom{
                padding: 1px 15px 3px 2px;
                border-radius: 50px;
            }
        </style>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <h2>Edição de: {{ $livro->titulo}}</h2>
        <hr>
        <form action="{{url('salva_livro')}}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{$livro->id}}">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputTit">Título</label>
                        <input type="text" class="form-control" name="titulo" value="{{$livro->titulo}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputAut">Autor</label>
                        <select name="id_autor" class="form-control">
                            @foreach ($autores as $autor)
                                @if ($livro->id_autor == $autor->id)
                                    <option value="{{ $autor->id }}" selected > {{ $autor->nome }}</option>
                                @else
                                    <option value="{{ $autor->id }}" >{{ $autor->nome }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputEdi">Editora</label>
                        <select name="id_editora" class="form-control">
                            @foreach ($editoras as $editora)
                                @if ($editora->id_editora == $editora->id)
                                    <option value="{{ $editora->id }}" selected > {{ $editora->nome }}</option>
                                @else
                                    <option value="{{ $editora->id }}" >{{ $editora->nome }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputLoc">Local</label>
                        <input type="text" class="form-control" name="local" value="{{$livro->local}}">
                    </div>
                </div>
                

                <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
        <script>

        </script>
        @endsection
    </body>