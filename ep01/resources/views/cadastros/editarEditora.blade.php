@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edição de Editoras</title>
        <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
        <style>
            .btn-custom{
                padding: 1px 15px 3px 2px;
                border-radius: 50px;
            }
        </style>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <h2>Edição de: {{ $editora->nome}}</h2>
        <hr>
        <form action="{{url('salva_editora')}}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{$editora->id}}">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="inputNome">Nome</label>
                        <input type="text" class="form-control" name="nome" value="{{$editora->nome}}">
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
        <script>

        </script>
        @endsection
    </body>