@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edição de Editoras</title>
        <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
        <style>
            .btn-custom{
                padding: 1px 15px 3px 2px;
                border-radius: 50px;
            }
        </style>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <div class="flrx-center position-ref full-height">
            <h2>Edição de Editoras</h2>
            <hr>
            @if ($message = Session::get('error'))
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">X</button>
                {!! $message !!}
            </div>
             @endif
            <br>
            <h4>Editoras</h4>
                <table border = "2px solid #7a7" cellpadding = "5px">
                <tr> 
                    <td>Id</td> 
                    <td>Nome</td>
                    <td>Ações</td> 
                 </tr>
                 @foreach ($editoras as $editora)
                 <tr>
                    <td>{{$editora->id}}</td> 
                    <td>{{$editora->nome}}</td> 
                    <td>
                        <button id="{{$editora->id}}" class="btn btn-info btn-custom btEditar">
                            <span class="fas fa-edit"></span>
                            Editar
                        </button>
                        <button id="{{$editora->id}}" class="btn btn-danger btn-custom btDeletar">
                            <span class="fas fa-trash btn-icon"></span>
                            Apagar
                        </button>
                    </td>
                 </tr>
                 @endforeach
                </table>
                <div>
                    <button id="btInserir" class="btn btn-primary btn-custom">
                        <span class="fas fa-plus-circle btn-icon"></span>
                        Inserir Novo
                    </button>
                </div>
            @endsection
            @section('scripts')
                <script>
                     $('.btEditar').click(function(){
                        var id = $(this).attr('id');
                        var url = "{{url('/')}}"+'/editar_editora/'+id;
                        //alert(url);
                        window.location.href = url;
                    });

                    $('#btInserir').click(function(){
                           var url = "{{url('inserir_nova_editora')}}";
                           //alert(url);
                           window.location.href = url;
                       });

                       $('.btDeletar').click(function(){
                           var id = $(this).attr('id');
                           var resp = confirm("Deseja excluir a Editora de Id: " + id +" ?");
                           if(resp){
                                var url = "{{url('/')}}"+'/deletar_editora/'+id;
                                 //alert(url);
                                window.location.href = url;
                           }
                           
                       });
                </script>
            @endsection
    </body>
</html>
