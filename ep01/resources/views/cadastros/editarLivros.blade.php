@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edição de Livros</title>
        <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
        <style>
            .btn-custom{
                padding: 1px 15px 3px 2px;
                border-radius: 50px;
            }
        </style>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <div class="flrx-center position-ref full-height">
            <h2>Edição de Livros</h2>
            <hr>
            <br>
            <h4>Livros</h4>
                <table border = "2px solid #7a7" cellpadding = "5px">
                <tr> 
                    <td>Nome do Livro</td> 
                    <td>Autor</td> 
                    <td>Editora</td> 
                    <td>Local</td>
                    <td>Ações</td>
                 </tr>
                 @foreach ($livros as $livro)
                 <tr>
                    <td>{{$livro->titulo}}</td> 
                    <td>{{$livro->autor->nome}}</td> 
                    <td>{{$livro->editora->nome}}</td> 
                    <td>{{$livro->local}}</td> 
                    <td>
                        <button id="{{$livro->id}}" class="btn btn-info btn-custom btEditar">
                            <span class="fas fa-edit"></span>
                            Editar
                        </button>
                        <button id="{{$livro->id}}" class="btn btn-danger btn-custom btDeletar">
                            <span class="fas fa-trash btn-icon"></span>
                            Apagar
                        </button>

                    </td>
                 </tr>
                 @endforeach
                </table>
                <div>
                    <button id="btInserir" class="btn btn-primary btn-custom">
                        <span class="fas fa-plus-circle btn-icon"></span>
                        Inserir Novo
                    </button>
                </div>
                @endsection
                @section('scripts')
                <script>
                    $('.btEditar').click(function(){
                       var id = $(this).attr('id');
                       var url = "{{url('/')}}"+'/editar_livro/'+id;
                       //alert(url);
                       window.location.href = url;
                   });

                   $('#btInserir').click(function(){
                           var url = "{{url('inserir_novo_livro')}}";
                           //alert(url);
                           window.location.href = url;
                       });

                       $('.btDeletar').click(function(){
                           var id = $(this).attr('id');
                           var resp = confirm("Deseja excluir o livro de Id: " + id +" ?");
                           if(resp){
                                var url = "{{url('/')}}"+'/deletar_livro/'+id;
                                 //alert(url);
                                window.location.href = url;
                           }
                           
                       });
               </script>
               @endsection
    </body>
</html>
