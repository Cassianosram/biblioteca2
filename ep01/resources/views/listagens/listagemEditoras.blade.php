@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Listagem de Editoras</title>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <div class="flrx-center position-ref full-height">
            <h2>Listagem de Editoras</h2>
            <hr>
            <br>
            <h4>Editoras</h4>
                <table border = "2px solid #7a7" cellpadding = "5px">
                <tr> 
                    <td>Id</td> 
                    <td>Nome</td> 
                 </tr>
                 @foreach ($editoras as $editora)
                 <tr>
                    <td>{{$editora->id}}</td> 
                    <td>{{$editora->nome}}</td> 
                 </tr>
                 @endforeach
                </table>
            @endsection
    </body>
</html>
