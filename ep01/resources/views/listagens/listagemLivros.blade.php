@extends('templates.template_base')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('estilos')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Listagem de Livros</title>
    </head>
    @endsection
    <body>
        @section('conteudo')
        <div class="flrx-center position-ref full-height">
            <h2>Listagem de Livros</h2>
            <hr>
            <br>
            <h4>Livros</h4>
                <table border = "2px solid #7a7" cellpadding = "5px">
                <tr> 
                    <td>Nome do Livro</td> 
                    <td>Autor</td> 
                    <td>Editora</td> 
                    <td>Local</td>
                 </tr>
                 @foreach ($livros as $livro)
                 <tr>
                    <td>{{$livro->titulo}}</td> 
                    <td>{{$livro->autor->nome}}</td> 
                    <td>{{$livro->editora->nome}}</td> 
                    <td>{{$livro->local}}</td> 
                 </tr>
                 @endforeach
                </table>
            @endsection
    </body>
</html>
